package controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import models.Team;
import org.junit.Before;
import org.junit.Test;
import play.mvc.Http.Response;
import play.test.FunctionalTest;

import java.util.*;

public class TeamsTest extends FunctionalTest {

    @Before
    public void auth() {
        Response response = POST("/application/auth", new HashMap<String, String>() {{
            put("email", "admin@admin.com");
            put("password", "admin");
        }});
        assertIsOk(response);
    }

    @Test
    public void getAll() {
        Response response = GET("/teams/getAll");
        Gson gson = new GsonBuilder().create();
        List<Map> list = gson.fromJson(getContent(response), List.class);
        assertTrue(list.size() > 0);
        assertEquals("team", list.get(0).get("name"));
    }


    @Test
    public void get() {
        Response response = POST("/teams/get", Collections.singletonMap("id", "1"));
        Gson gson = new GsonBuilder().create();
        Team team = gson.fromJson(getContent(response), Team.class);
        assertEquals("team", team.name);

        Response response1 = POST("/teams/get", Collections.singletonMap("id", "9999"));
        assertStatus(404, response1);
    }

    @Test
    public void create() {
        long count = Team.count();
        Response response = POST("/teams/create", new HashMap<String, String>() {{
            put("name", "test");
            put("description", "testtralala");
        }});
        Gson gson = new GsonBuilder().create();
        Map map = gson.fromJson(getContent(response), Map.class);
        assertEquals(count + 1, ((Double) map.get("id")).longValue());
    }

    @Test
    public void join() {
        Response response = POST("/teams/join", Collections.singletonMap("id", "1"));
        assertStatus(403, response);

        response = POST("/teams/join", Collections.singletonMap("id", "2"));
        assertIsOk(response);
    }

}