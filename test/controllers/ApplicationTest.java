package controllers;

import org.junit.*;
import play.test.*;
import play.mvc.*;
import play.mvc.Http.*;

import java.util.HashMap;
//import models.*;

public class ApplicationTest extends FunctionalTest {

    @Test
    public void auth() {
        Response response = POST("/application/auth", new HashMap<String, String>() {{
            put("email", "admin@admin.com");
            put("password", "admin");
        }});
        assertIsOk(response);
//        assertContentType("text/html", response);
//        assertCharset(play.Play.defaultWebEncoding, response);
    }
    
}