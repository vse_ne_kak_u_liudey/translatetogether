package controllers;

import com.ning.http.client.ws.WebSocket;
import models.*;
import net.sf.oval.constraint.MaxLength;
import play.*;
import play.data.validation.*;
import play.db.jpa.Blob;
import play.mvc.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.*;

@With(TestController.class)
public class Application extends Controller {

    public static final String USER = "user";

    public static void index() {
        render();
    }

    public static void register() {
        render();
    }

    public static void registerFinish(@Required @Email String email,
                                      @Required @MinSize(5) String password,
                                      @Equals("password") String password2,
                                      @Required String name) {
        if(User.findByEmail(email)!=null) {
            Validation.addError("email", "такой пользователь уже существует");
        }
        if (validation.hasErrors()) {
            validation.keep();
            params.flash();
            flash.error("Please correct these errors !");
            register();
        }
        new User(email, password, name);
        login();
    }

    public static void login() {
        render();
    }

    public static void auth(@Required @Email String email,
                            @Required @MinSize(5) String password) {
        User user = User.find("email", email).first();
        if (user != null && password.equals(user.password)) {
            session.put("user", user.id);
            ok();
        }
        badRequest("Неверные логин или пароль");
    }

    public static void authHtml(@Required @Email String email,
                            @Required @MinSize(5) String password) {
        User user = User.find("email", email).first();
        if (user != null && password.equals(user.password)) {
            session.put(USER, user.id);
            main();
        }
        flash.error("Неверные логин или пароль");
        login();
    }

    public static void main() {
        User user = getUser();
        render();
    }

    protected static User getUser() {
        if (!session.contains(USER)) {
            flash.error("Пользователь не найден");
            login();
        }
        return User.findById(Long.valueOf(session.get(USER)));
    }

    public static void mainNewProjectNextSeries(long projectId, long teamId){
        Project project = Project.findById(projectId);

        Project newProject = new Project(project.name, project.chief, project.country, project.year, project.genre,
                project.starring, project.description, project.countSeries, project.type);
        newProject.picture = project.picture;
        newProject.numberSeries = project.numberSeries + 1;
        newProject.save();

        mainProject(newProject.id);
    }

    public static void mainProject(long projectId) {
        User user = getUser();
        Project project = Project.findById(projectId);
        List<Team> teams = Team.findAll();
        List<Team> userTeams = Team.findByUser(user);
        List<Project> projects = Project.findAll();
        List<Project> userProjects = Project.findByUser(user);

        int  done = 0, reserve = 0, free = 0;
        if (project.subtitleCount > 0) {
            for (ProjectProgress progress : project.progresses) {
                int range = progress.end - progress.start + 1;
                if (progress.isDone){
                    done += range;
                }
                else {
                    reserve += range;
                }
            }
            done = done * 100 / project.subtitleCount;
            reserve = reserve * 100 / project.subtitleCount;
            free = 100 - done - reserve;
        }
        project.done = done;
        project.reserve = reserve;
        project.save();

        render(user, project, userTeams, projects, userProjects, teams, done, reserve, free);
    }

    public static void logout() {
        flash.success("You've been logged out");
        session.clear();
        ok();
    }

    public static void enterChat(User user) {
            ChatWebSocket.chat(user.name);
    }

}
