package controllers;

import play.mvc.Before;
import play.mvc.Controller;

/**
 * Created by TuMoH on 12.06.2016.
 */
public class TestController extends Controller {

    @Before
    public static void setAccessControl() {
        response.setHeader("Access-Control-Allow-Origin", "*");
//        response.setHeader("Access-Control-Allow-Credentials", "true");
//        response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
//        response.setHeader("Access-Control-Allow-Methods", "POST, PUT, GET, DELETE, OPTIONS");
//        response.setHeader("Access-Control-Expose-Headers", "content-type,X-Pagination-Current-Page, X-Pagination-Page-Count, X-Pagination-Per-Page, X-Pagination-Total-Count");
    }

}
