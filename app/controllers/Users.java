package controllers;

import com.google.gson.*;
import models.Project;
import models.ProjectProgress;
import models.Team;
import models.User;
import org.apache.commons.collections.map.HashedMap;
import play.db.jpa.Blob;
import play.mvc.Controller;
import play.mvc.With;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Veronika on 24.05.2016.
 */
@With(TestController.class)
public class Users extends Controller {

    public static JsonSerializer simpleUserSerializer = new JsonSerializer<User>() {
        @Override
        public JsonElement serialize(User user, Type type, JsonSerializationContext jsonSerializationContext) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.add("id", new JsonPrimitive(user.id));
            jsonObject.add("name", new JsonPrimitive(user.name));
            return jsonObject;
        }
    };

    public static void profile(){
        User user = User.findById(Long.valueOf(session.get("user")));
        List<ProjectProgress> projectProgress = ProjectProgress.findByUser(user);
        int done = 0;
        for (ProjectProgress projectProgres : projectProgress) {
            int range = projectProgres.end - projectProgres.start + 1;
                done += range;
        }
        Map<String, Object> response = new HashMap<>();
        response.put("user", user);
        response.put("done", done);
        renderJSON(response);
    }

    public static void setPicture(File picture) {
        User user = User.findById(Long.valueOf(session.get("user")));
        if (user != null && picture != null) {
            Blob blob = new Blob();
            try {
                blob.set(new FileInputStream(picture), "application/octet-stream");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            user.picture = blob;
            user.save();
        }
        profile();
    }

    public static void getPicture() {
        User user = User.findById(Long.valueOf(session.get("user")));
        response.setContentTypeIfNotSet(user.picture.type());
        renderBinary(user.picture.get());
    }

    public static void getInvitedTeams(){
        User user = User.findById(Long.valueOf(session.get("user")));
        List<Team> teams = Team.findByUserInvited(user);
        renderJSON(teams, simpleUserSerializer);
    }

    public static void addUser(long id){
        User user = User.findById(Long.valueOf(session.get("user")));
        Team team = Team.findById(id);
        team.addUser(user);
        team.invited.remove(user);
        team.save();
        ok();
    }

}