package controllers;

import play.libs.F;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.WebSocketController;

import static play.libs.F.Matcher.ClassOf;
import static play.libs.F.Matcher.Equals;
import static play.mvc.Http.WebSocketEvent.SocketClosed;
import static play.mvc.Http.WebSocketEvent.TextFrame;

import models.*;
import play.mvc.With;

@With(TestController.class)
public class ChatWebSocket extends Controller {

    public static void chat(String user) {
        render(user);
    }

    public static class ChatRoomSocket extends WebSocketController {

        public static void join() {
            User user = User.findById(Long.valueOf(session.get("user")));
            Chat chat = Chat.get();

            // Socket connected, join the chat room
            F.EventStream<Chat.Event> roomMessagesStream = chat.join(user);

            // Loop while the socket is open
            while(inbound.isOpen()) {

                // Wait for an event (either something coming on the inbound socket channel, or ChatRoom messages)
                F.Either<Http.WebSocketEvent,Chat.Event> e = await(F.Promise.waitEither(
                        inbound.nextEvent(),
                        roomMessagesStream.nextEvent()
                ));

                // Case: User typed 'quit'
                for(String userMessage: TextFrame.and(Equals("quit")).match(e._1)) {
                    chat.leave(user);
                    outbound.send("quit:ok");
                    disconnect();
                }

                // Case: TextEvent received on the socket
                for(String userMessage: TextFrame.match(e._1)) {
                    chat.say(user, userMessage);
                }

                // Case: Someone joined the room
                for(Chat.Join joined: ClassOf(Chat.Join.class).match(e._2)) {
                    outbound.send("join:%s", joined.user);
                }

                // Case: New message on the chat room
                for(Chat.Message message: ClassOf(Chat.Message.class).match(e._2)) {
                    outbound.send("message:%s:%s", message.user, message.text);
                }

                // Case: Someone left the room
                for(Chat.Leave left: ClassOf(Chat.Leave.class).match(e._2)) {
                    outbound.send("leave:%s", left.user);
                }

                // Case: The socket has been closed
                for(Http.WebSocketClose closed: SocketClosed.match(e._1)) {
                    chat.leave(user);
                    disconnect();
                }

            }

        }

    }

}

