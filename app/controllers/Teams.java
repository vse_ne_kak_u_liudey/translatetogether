package controllers;

import com.google.gson.*;
import models.Member;
import models.Project;
import models.Team;
import models.User;
import play.Play;
import play.data.validation.MinSize;
import play.data.validation.Required;
import play.db.jpa.Blob;
import play.mvc.Controller;
import play.mvc.With;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Veronika on 10.06.2016.
 */


@With(TestController.class)
public class Teams extends Controller {

    public static JsonSerializer simpleTeamSerializer = new JsonSerializer<Team>() {
        @Override
        public JsonElement serialize(Team team, Type type, JsonSerializationContext jsonSerializationContext) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.add("id", new JsonPrimitive(team.id));
            jsonObject.add("name", new JsonPrimitive(team.name));
            return jsonObject;
        }
    };

    public static void create(@Required @MinSize(3) String name, @Required String description) {
        if(Team.findByName(name)!=null) {
            badRequest("Ошибка! команда уже существует");
        }
        if (validation.hasErrors()) {
            validation.keep();
            badRequest("Ошибка! \n" + "name: " + name + " , description: " + description + "\n" + validation.errorsMap());
        }
        User chief = User.findById(Long.valueOf(session.get("user")));
        Team team = new Team(name, chief, description);
        renderJSON(Collections.singletonMap("id", team.id));
    }

    public static void join(long id) {
        User user = User.findById(Long.valueOf(session.get("user")));
        Team team = Team.findById(id);
        if (team == null) {
            notFound("Команда не найдена");
        }
        if (team.getMember(user) != null) {
            forbidden("Вы уже состоите в этой команде");
        }
        team.askedUser(user);
        ok();
    }

    public static void invite(long id) {
        User user = User.findById(Long.valueOf(session.get("user")));
        Team team = Team.findById(id);
        Member member = team.getMember(user);
        if (team == null) {
            notFound("Команда не найдена");
        }
        if (team.getMember(user) != null) {
            forbidden("Этот человек уже состоит в этой команде");
        }
        if (!Member.CHIEF.equals(member.role)){
            forbidden("У вас нет прав");
        }
        team.invitedUser(user);
        ok();
    }

    public static void getAskedUsers(long id){
        User user = User.findById(Long.valueOf(session.get("user")));
        Team team = Team.findById(id);
        renderJSON(team.asked, Users.simpleUserSerializer);
    }

    public static void addUser(long id){
        User user = User.findById(Long.valueOf(session.get("user")));
        Team team = Team.findById(id);
        team.addUser(user);
        team.asked.remove(user);
        team.save();
        ok();
    }

    public static void exit(long id) {
        User user = User.findById(Long.valueOf(session.get("user")));
        Team team = Team.findById(id);
        if (team == null) {
            notFound("Команда не найдена");
            return;
        }
        Member member = team.getMember(user);
        if (member == null) {
            forbidden("Вы не состоите в этой команде");
            return;
        }
        if (Member.USER.equals(member.role)) {
            team.deleteMember(member);
            List<Project> projects = Project.findByTeam(team);
            for (Project project : projects) {
                project.deleteUser(user);
            }
        }
        else forbidden("У вас нет прав");
    }

    public static void getByUser() {
        if (!session.contains("user")){
            forbidden("не авторизован");
        }
        User user = User.findById(Long.valueOf(session.get("user")));
        List<Team> teams = Team.findByUser(user);
        renderJSON(teams, simpleTeamSerializer);
    }

    public  static void get(Long id){
        Team team = Team.findById(id);
        if (team == null){
            notFound("Не найден");
        }
        renderJSON(team, Users.simpleUserSerializer);
    }

    public static void getAll(){
        List<Team> teams = Team.findAll();
        renderJSON(teams, simpleTeamSerializer);
    }

    public static void setPicture(Long id, File picture) {
        Team team = Team.findById(id);
        if (team != null && picture != null) {
            Blob blob = new Blob();
            try {
                blob.set(new FileInputStream(picture), "application/octet-stream");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            team.picture = blob;
            team.save();
        }
    }

    public static void getPicture(long id) {
        Team team = Team.findById(id);
        response.setContentTypeIfNotSet(team.picture.type());
        renderBinary(team.picture.get());
    }

    public static void invite() {

    }

}
