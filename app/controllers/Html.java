package controllers;

import models.Project;
import models.Team;
import models.User;

import java.util.List;

public class Html extends Application {

    public static void main() {
        User user = getUser();
        render(user);
    }

    public static void mainMyTeams() {
        User user = getUser();
        List<Team> teams = Team.findAll();
        List<Team> userTeams = Team.findByUser(user);
        List<Project> projects = Project.findAll();
        List<Project> userProjects = Project.findByUser(user);
        String title = "Мои команды";
        render(user, teams, title, userTeams, projects, userProjects);
    }

    public static void mainAllTeams() {
        User user = getUser();
        List<Team> teams = Team.findAll();
        List<Team> userTeams = Team.findByUser(user);
        List<Project> projects = Project.findAll();
        List<Project> userProjects = Project.findByUser(user);
        String title = "Все команды";
        render(user, teams, title, userTeams, projects, userProjects);
    }

    public static void mainAllProjects() {
        User user = getUser();
        List<Team> teams = Team.findAll();
        List<Team> userTeams = Team.findByUser(user);
        List<Project> projects = Project.findAll();
        List<Project> userProjects = Project.findByUser(user);
        String title = "Все проекты";
        render(user, projects, title, userTeams, projects, userProjects, teams);
    }

    public static void mainNewProject(long teamId){
        User user = getUser();
        List<Team> teams = Team.findAll();
        List<Team> userTeams = Team.findByUser(user);
        List<Project> projects = Project.findAll();
        List<Project> userProjects = Project.findByUser(user);
        render(user, userTeams, projects, userProjects, teams, teamId);
    }

    public static void mainNewProjectNextSeries(long projectId, long teamId){
        Project project = Project.findById(projectId);

        Project newProject = new Project(project.name, project.chief, project.country, project.year, project.genre,
                project.starring, project.description, project.countSeries, project.type);
        newProject.picture = project.picture;
        newProject.numberSeries = project.numberSeries + 1;
        newProject.save();

        mainProject(newProject.id);
    }

    public static void mainNewTeam(){
        User user = getUser();
        List<Team> teams = Team.findAll();
        List<Team> userTeams = Team.findByUser(user);
        List<Project> projects = Project.findAll();
        List<Project> userProjects = Project.findByUser(user);
        render(user, userTeams, projects, userProjects, teams);
    }

    public static void mainTeam(long teamId) {
        User user = getUser();
        Team team = Team.findById(teamId);
        List<Team> teams = Team.findAll();
        List<Team> userTeams = Team.findByUser(user);
        List<Project> projects = Project.findAll();
        List<Project> teamProjects = Project.findByTeam(team);
        List<Project> userProjects = Project.findByUser(user);
        render(user, userTeams, projects, userProjects, teams, team, teamProjects);
    }

}
