package controllers;

import com.google.gson.*;
import com.timursoft.subtitleparser.*;
import models.*;
import play.Play;
import play.data.validation.MinSize;
import play.data.validation.Required;
import play.data.validation.Validation;
import play.db.jpa.Blob;
import play.mvc.Controller;
import play.mvc.With;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.Type;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static java.lang.Integer.parseInt;
import static java.lang.Integer.valueOf;

/**
 * Created by Veronika on 10.06.2016.
 */

@With(TestController.class)
public class Projects extends Controller {

    public static final String SUBTITLE_DIR = "E:\\Play\\TranslateTogether\\db\\subtitle";

    public static JsonSerializer simpleProjectSerializer = new JsonSerializer<Project>() {
        @Override
        public JsonElement serialize(Project project, Type type, JsonSerializationContext jsonSerializationContext) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.add("id", new JsonPrimitive(project.id));
            jsonObject.add("name", new JsonPrimitive(project.name));
            return jsonObject;
        }
    };

    public static void create(@Required String name, @Required String description, @Required String country,
                                  @Required int year, @Required String genre, @Required String starring, @Required int countSeries,
                                  @Required String type, long teamId, int numberSeries) {
        if (validation.hasErrors()) {
            validation.keep();
            badRequest("Ошибка");
        }
        User chief = User.findById(Long.valueOf(session.get("user")));
        Team team = Team.findById(teamId);
        Project project = new Project(name, chief, country, year, genre, starring, description, countSeries, type, team);
        project.numberSeries = numberSeries > 0 ? numberSeries : 1;
        project.save();
        renderJSON(Collections.singletonMap("id", project.id));
    }

    public static void join(long id) {
        User user = User.findById(Long.valueOf(session.get("user")));
        Project project = Project.findById(id);

        if (project == null) {
            notFound("Проект не найден");
        }
        if (project.getPartner(user) != null){
            forbidden("Вы уже состоите в этом проекте");
        }
        project.addUser(user, Role.TRANSLATOR);
        ok();
    }

    public static void exit(long id) {
        User user = User.findById(Long.valueOf(session.get("user")));
        Project project = Project.findById(id);
        if (project == null) {
            notFound("Проект не найден");
        }
        Partner partner = project.getPartner(user);
        if (partner == null) {
            forbidden("Вы не состоите в этом проекте");
            return;
        }
        project.deletePartner(partner);
        ok();
    }

    public static void changeRoles(long id, String... roles){
        User user = User.findById(Long.valueOf(session.get("user")));
        Project project = Project.findById(id);
        Partner partner = project.getPartner(user);

        boolean isChief = partner.roles.contains(Role.CHIEF);
        partner.roles.clear();
        if (isChief){
            partner.roles.add(Role.CHIEF);
        }
        for (String sRole : roles) {
            Role role = Role.find("name", sRole.toUpperCase()).first();
            if (Role.CHIEF.equals(role)){
                forbidden("Нельзя менять шефа");
            }
            if (role != null){
                partner.roles.add(role);
            }
            else {
                forbidden("Неправильно задана роль");
            }
        }
        partner.save();
        ok();
    }

    public static void test(){
        ok();
    }

    public static void get(Long id) {
        Project project = Project.findById(id);
        if (project == null) {
            notFound("Не найден");
        }
        renderJSON(project, Users.simpleUserSerializer, Teams.simpleTeamSerializer);
    }

    public static void getAll(){
        List<Project> projects = Project.findAll();
        renderJSON(projects, simpleProjectSerializer);
    }

    public static void getByUser() {
        if (!session.contains("user")){
            forbidden("не авторизован");
        }
        User user = User.findById(Long.valueOf(session.get("user")));
        List<Project> projects = Project.findByUser(user);
        renderJSON(projects, simpleProjectSerializer);
    }


    public static void getByTeam(Long teamId) {
        Team team = Team.findById(teamId);
        if (team == null) {
            renderJSON(Collections.EMPTY_LIST);
        }
        List<Project> projects = Project.findByTeam(team);
        renderJSON(projects, simpleProjectSerializer);
    }

    public static void setPicture(Long id, File picture) {
        Project project = Project.findById(id);
        if (project != null && picture != null) {
            Blob blob = new Blob();
            try {
                blob.set(new FileInputStream(picture), "application/octet-stream");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            project.picture = blob;
            project.save();
        }
    }

    public static void getPicture(long id) {
        Project project = Project.findById(id);
        response.setContentTypeIfNotSet(project.picture.type());
        renderBinary(project.picture.get());
    }

    public static void setSubtitle(Long id, File subtitle) {
        User user = User.findById(Long.valueOf(session.get("user")));
        Project project = Project.findById(id);

        if (subtitle == null || subtitle.length() > (1 * 1024 * 1024)) {
            // TODO: 04.07.2016 выдать ошибку
            Application.mainProject(project.id);
        }
        if (!subtitle.getName().matches(".*\\.(ass|ssa|srt)$")) {
            // TODO: 04.07.2016  выдать ошибку
            Application.mainProject(project.id);
        }

        if (project != null && subtitle != null) {
            File dir = new File(SUBTITLE_DIR + File.separator + user.name);
            dir.mkdirs();
            File file = new File(dir, subtitle.getName());
            subtitle.renameTo(file);
            project.subtitle = file.getAbsolutePath();

            Format format = null;
            if (file.getName().endsWith(".srt")) {
                format = new FormatSRT();
            } else if (file.getName().matches(".*\\.(ass|ssa)$")) {
                format = new FormatASS();
            }
            if (format != null) {
                try {
                    SubtitleObject subtitleObject = format.parse(IOHelper.streamToString(new FileInputStream(file)));
                    project.subtitleCount = subtitleObject.subtitles.size();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
            project.save();
        }
        Application.mainProject(project.id);
    }

    public static void reserveLines(int end, long id) {
        User user = User.findById(Long.valueOf(session.get("user")));
        Project project = Project.findById(id);

        int start = 1;
        if (!project.progresses.isEmpty()) {
            start = project.progresses.get(project.progresses.size() - 1).end + 1;
        }
        if (start > end) {
            flash.error("Конец должен быть больше начала!");
            Application.mainProject(project.id);
        }
        if (end > project.subtitleCount) {
            flash.error("Неправильно выбран диапазон!");
            Application.mainProject(project.id);
        }

        ProjectProgress projectProgress = new ProjectProgress(start, end, user);
        project.progresses.add(projectProgress);
        project.save();
        Application.mainProject(project.id);
    }

    public static void reserveToDone(long id, long progressId){
        Project project = Project.findById(id);
        ProjectProgress projectProgress = ProjectProgress.findById(progressId);
        projectProgress.isDone = true;
        projectProgress.save();
        Application.mainProject(project.id);
    }

    public static void deleteAll() {
        Project.deleteAll();
        Application.main();
    }

}




