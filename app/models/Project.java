package models;

import play.Play;
import play.db.jpa.Blob;
import play.db.jpa.Model;

import javax.persistence.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Veronika on 06.06.2016.
 */

@Entity
public class Project extends Model{

    public  String name;
    @ManyToOne
    public User chief;

    @OneToMany(cascade = CascadeType.ALL)
    public List<Partner> partners = new ArrayList<>();

    @ManyToOne
    public Team team;

    public String country;
    public int year;
    public String genre;
    public String starring;
    public int countSeries;
    public Blob picture;
    public String subtitle;
    public int subtitleCount;
    public int reservePercent;
    public int donePercent;
    public int numberSeries;
    public int done;
    public int reserve;

    @Column( length = 100000 )
    public String description;
    public String type;

    @OneToMany
    public List<ProjectProgress> progresses = new ArrayList<>();

    public Project(String name, User chief, String country, int year, String genre, String starring,
                   String description, int countSeries, String type) {
        this(name, chief, country, year, genre, starring, description, countSeries, type, null);
    }

    public Project(String name, User chief, String country, int year, String genre, String starring,
                   String description, int countSeries, String type, Team team) {
        this.name = name;
        this.partners.add(new Partner(chief, Role.CHIEF));
        this.team = team;
        this.description = description;
        this.country = country;
        this.year = year;
        this.genre = genre;
        this.starring = starring;
        this.countSeries = countSeries;
        this.type = type;
        create();
    }

    public void addUser(User user, Role role) {
        partners.add(new Partner(user, role));
        save();
    }


    public static List<Project> findByUser(User user) {
        return Project.find("select distinct pr from Project pr, User u, Partner p  where u.name = ?1 and u = p.user and p member of pr.partners", user.name).fetch();
    }

    public static List<Project> findByTeam(Team team) {
        return Project.find("select distinct p from Project p, Team t where t.id = ?1 and t = p.team", team.id).fetch();
    }

    public Partner getPartner(User user) {
        return partners.stream().filter(m -> m.user.equals(user)).findFirst().orElse(null);
    }

    public void deleteUser(User user) {
        Partner partner = getPartner(user);
        deletePartner(partner);
    }

    public void deletePartner(Partner partner) {
        partners.remove(partner);
        save();
    }

}
