package models;

import play.Play;
import play.db.jpa.Blob;
import play.db.jpa.Model;

import javax.jws.soap.SOAPBinding;
import javax.persistence.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Veronika on 03.06.2016.
 */
@Entity
public class Team extends Model {

    public String name;

    @OneToMany(cascade = CascadeType.ALL)
    public List<Member> members = new ArrayList<>();

    @ManyToMany
//    @JoinTable(name="User")
    public List<User> invited = new ArrayList<>();

    @ManyToMany
//    @JoinTable(name="User")
    public List<User> asked = new ArrayList<>();

    @Column( length = 100000 )
    public String description;

    public Blob picture;

    public Team(String name, User chief, String description) {
        this.name = name;
        this.description = description;
        this.members.add(new Member(chief, Member.CHIEF));
        this.picture = new Blob();
        try {
            File file = Play.getFile("/public/images/notAvatar.jpg");
            this.picture.set(new FileInputStream(file), "application/octet-stream");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        create();
    }

    public void addUser(User user) {
        members.add(new Member(user, Member.USER));
        save();
    }

    public void askedUser(User user){
        asked.add(user);
        save();
    }

    public void invitedUser(User user){
        invited.add(user);
        save();
    }

    public static List<Team> findByUser(User user) {
        return Team.find("select distinct t from Team t, User u, Member m where u.name = ?1 and u = m.user and m member of t.members", user.name).fetch();
    }

    public static List<Team> findByUserInvited(User user){
        return Team.find("select distinct t from Team t, User u where u.name = ?1 and u member of t.invited", user.name).fetch();
    }

    public Member getMember(User user) {
        return members.stream().filter(m -> m.user.equals(user)).findFirst().orElse(null);
    }

    public void deleteMember(Member member) {
        members.remove(member);
        save();
    }

    public static Team findByName(String name) {
        return find("name", name).first();
    }

}
