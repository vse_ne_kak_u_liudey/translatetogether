package models;

import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.List;

/**
 * Created by Veronika on 05.08.2016.
 */
@Entity
public class Member extends Model{

    public static final String CHIEF = "CHIEF";
    public static final String ADMIN = "ADMIN";
    public static final String USER = "USER";

    @ManyToOne
    public User user;
    public String role;

    public Member(User user, String role) {
        this.user = user;
        this.role = role;
    }

    public static List<Member> findByTeam(Team team) {
        return Member.find("select distinct m from Member m, Team t where t.id = ?1 and m member of t.members", team.id).fetch();
    }


}
