package models;

import play.db.jpa.Model;

import javax.persistence.*;
import java.util.*;

/**
 * Created by Veronika on 05.08.2016.
 */
@Entity
public class Partner extends Model{

    @ManyToOne
    public User user;

//    @ManyToOne(targetEntity=Project.class)
//    public List<String> roles = new ArrayList<>();

    @ManyToMany
    public Set<Role> roles = new HashSet<>();

    public Partner(User user, Role... role) {
        this.user = user;
        this.roles.addAll(Arrays.asList(role));
    }

}
