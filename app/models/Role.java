package models;

import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Veronika on 05.08.2016.
 */
@Entity
public class Role extends Model{

    public static Role CHIEF;
    public static Role TRANSLATOR;
    public static Role EDITOR;
    public static Role TIMING;

    public String name;

    public Role(String name) {
        this.name = name;
        create();
    }
}
