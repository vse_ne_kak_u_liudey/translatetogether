package models;

import play.Play;
import play.db.jpa.Blob;
import play.db.jpa.Model;

import javax.persistence.Entity;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;


@Entity
public class User extends Model {

    public String email;
    public String password;
    public String name;
    public Blob picture;

    public User(String email, String password, String name) {
        this.email = email;
        this.password = password;
        this.name = name;
        Blob blob = new Blob();
        try {
            File file = Play.getFile("/public/images/noava.jpg");
            blob.set(new FileInputStream(file), "application/octet-stream");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        this.picture = blob;
        create();
    }

    public static User findByEmail(String email) {
        return find("email", email).first();
    }

}
