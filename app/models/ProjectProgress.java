package models;

import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.List;

/**
 * Created by Veronika on 05.07.2016.
 */

@Entity
public class ProjectProgress extends Model {

    public int start;
    public int end;

    @ManyToOne
    public User user;

    public boolean isDone;

    public ProjectProgress(int start, int end, User user) {
        this.start = start;
        this.end = end;
        this.user = user;
        create();
    }

    public static List<ProjectProgress> findByUser(User user) {
        return ProjectProgress.find("select distinct p from ProjectProgress p, User u where u.name = ?1 and u = p.user and p.isDone = true", user.name).fetch();
    }
}
