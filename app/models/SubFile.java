package models;

import play.db.jpa.Blob;
import play.db.jpa.Model;

import javax.persistence.Entity;

/**
 * Created by Veronika on 11.06.2016.
 */

@Entity
public class SubFile extends Model {

    public String name;
    public Blob file;
}
