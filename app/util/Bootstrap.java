package util;

import models.Project;
import models.Role;
import models.Team;
import models.User;
import play.jobs.Job;
import play.jobs.OnApplicationStart;
import play.test.Fixtures;

@OnApplicationStart
public class Bootstrap extends Job {

    public void doJob() {
        if (Role.count() == 0) {
            Role.CHIEF = new Role("CHIEF");
            Role.TRANSLATOR = new Role("TRANSLATOR");
            Role.EDITOR = new Role("EDITOR");
            Role.TIMING = new Role("TIMING");
        } else {
            Role.CHIEF = Role.find("name", "CHIEF").first();
            Role.TRANSLATOR = Role.find("name", "TRANSLATOR").first();
            Role.EDITOR = Role.find("name", "EDITOR").first();
            Role.TIMING = Role.find("name", "TIMING").first();
        }

        if (User.count() == 0) {
            new User("admin@admin.com", "admin", "admin");
            new User("user1@user1.com", "user1", "user1");
            new User("user2@user2.com", "user2", "user2");
            new User("user3@user3.com", "user3", "user3");
        }
        if (Team.count() == 0) {
            new Team("team", User.findByEmail("admin@admin.com"), "tralala");
            new Team("pampam", User.findByEmail("user1@user1.com"), "trampampam");
            new Team("mango", User.findByEmail("user1@user1.com"), "mamarama");
            new Team("carabas", User.findByEmail("admin@admin.com"), "carabas tarabas");
        }
        if (Project.count() == 0) {
            new Project("Doctors", User.findByEmail("admin@admin.com"), "Южная Корея", 2016, "медицина", "Ю Хэ Джон",
                    "тра та та", 16, "serial");
            new Project("Луна", User.findByEmail("admin@admin.com"), "Китай", 2016, "детектив", "Ю Хэ Джон",
                    "лунный свет", 16, "serial", Team.findByName("team"));
            new Project("Солнце", User.findByEmail("user1@user1.com"), "Таиланд", 2016, "романтика", "Ю Хэ Джон",
                    "солнце светит", 16, "film");
            new Project("Друзья", User.findByEmail("user1@user1.com"), "Южная Корея", 2016, "комедия", "Ю Хэ Джон",
                    "мы с Тамарой ходим парой", 16, "serial", Team.findByName("carabas"));
        }
    }

}